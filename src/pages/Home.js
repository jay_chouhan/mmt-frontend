import {
  AppBar,
  Toolbar,
  Container,
  Typography,
  makeStyles,
} from "@material-ui/core";
import Login from "../components/Login";
import Signup from "../components/Signup";

import FilghtSearch from "../components/FilghtSearch";
import InfoCard from "../components/InfoCard";
import FlightTakeoffIcon from "@material-ui/icons/FlightTakeoff";
import LanguageIcon from "@material-ui/icons/Language";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import BlogList from "../components/BlogList";
import isEmpty from "../components/utils/isEmpty";
import LoggedInButton from "../components/LoggedInButton";

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundImage: "linear-gradient(to bottom,#051322,#15457c)",
    paddingBottom: "2rem",
    position: "relative",

    [theme.breakpoints.down("sm")]: {
      paddingBottom: 0,
    },
  },
  navbar: {
    backgroundColor: "transparent",
    boxShadow: "none",
  },
  logo: {
    width: "125px",
  },
  toolBar: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  navLinks: {
    display: "flex",
    gap: "1rem",
  },
  infoWrapper: {
    display: "flex",
    gap: "1rem",
    margin: "2rem 0",

    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      margin: "1rem",
    },
  },
  icon: {
    fontSize: "4rem",
  },
  footer: {
    backgroundColor: "#131212",
  },
  footerText: {
    color: "#fff",
    textAlign: "right",
    padding: "1rem",
    fontWeight: "bold",
  },
}));

const Home = (props) => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.header}>
        <Container maxWidth="lg">
          <AppBar position="static" className={classes.navbar}>
            <Toolbar className={classes.toolBar}>
              <img
                src="/mmtLogoWhite.png"
                alt="logo"
                className={classes.logo}
              />
              <div className={classes.navLinks}>
                {isEmpty(props.user) ? (
                  <>
                    <Login verifyUser={props.verifyUser} />
                    <Signup verifyUser={props.verifyUser} />
                  </>
                ) : (
                  <LoggedInButton user={props.user} logout={props.logout} />
                )}
              </div>
            </Toolbar>
          </AppBar>
          <FilghtSearch />
        </Container>
      </div>

      <Container maxWidth="lg" disableGutters>
        <div className={classes.infoWrapper}>
          <InfoCard
            icon={
              <FlightTakeoffIcon className={classes.icon} color="primary" />
            }
          />
          <InfoCard
            icon={<LanguageIcon className={classes.icon} color="primary" />}
          />
          <InfoCard
            icon={<LoyaltyIcon className={classes.icon} color="primary" />}
          />
        </div>
        <BlogList />
      </Container>
      <div className={classes.footer}>
        <Container maxWidth="lg">
          <Typography className={classes.footerText}>
            &copy; 2021 MakeMyTrip PVT.LTD.
          </Typography>
        </Container>
      </div>
    </>
  );
};

export default Home;
