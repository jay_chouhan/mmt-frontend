import {
  Button,
  Card,
  Container,
  FormHelperText,
  Grid,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import server from "../api/server";
import FlightItem from "../components/FlightItem";
import Header from "../components/Header";
import PassengerDetailForm from "../components/PassengerDetailForm";
import isEmpty from "../components/utils/isEmpty";
import queryString from "query-string";
import Loader from "../components/Loader";

const styles = (theme) => ({
  heading: {
    background: "#0a223d",
    color: "#fff",
    padding: "1rem",
    borderRadius: 0,
  },
  cardTitle: {
    color: "#fff",
    margin: "1rem auto",
  },
  flightDetails: {
    padding: "1rem",
  },
  fareDetails: {
    padding: "1rem",

    "& > div": {
      borderBottom: "1px solid #ccc",
      margin: "0.5rem 0",
      padding: "0.5rem",
    },
  },
  subtitle: {
    fontSize: "0.9rem",
    color: "#777",
  },
  shortInfo: {
    padding: "1rem 0 0 1rem",
  },
  addMoreButton: {
    margin: "1rem",
  },
  continueButton: {
    display: "block",
    marginLeft: "auto",
  },
  error: {
    color: "#f44336",
  },
  notFound: {
    padding: "1rem",
    textAlign: "center",
  },
});

class Review extends Component {
  state = {
    travellerDetails: [
      {
        firstName: "",
        lastName: "",
        gender: "",
      },
    ],
    travellers: 1,
    errors: {
      travellerError: [],
      generic: "",
    },
    flightDetails: null,
    bookingDetails: null,
    status: "loading",
  };

  componentDidMount() {
    const { bookingId } = queryString.parse(this.props.location.search);

    const accessToken = localStorage.getItem("accessToken");

    server
      .get(`/flights/booking/${bookingId}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        this.setState({
          flightDetails: res.data.flightDetails,
          bookingDetails: res.data.getBooking,
          status: "loaded",
        });
      })
      .catch((error) => {
        this.setState({
          errors: error || error.response.data,
          status: "failed",
        });
      });
  }

  addMoreTraveller = () => {
    this.setState((state) => {
      if (state.travellers !== state.bookingDetails.num_passengers) {
        const list = [...state.travellerDetails];
        list.push({
          firstName: "",
          lastName: "",
          gender: "",
        });
        return {
          travellers: state.travellers + 1,
          travellerDetails: list,
        };
      }
    });
  };

  handleChange = (index) => {
    return (name, value) => {
      this.setState((state) => {
        const list = state.travellerDetails;
        list[index][name] = value;
        return {
          travellerDetails: list,
        };
      });
    };
  };

  onContinue = () => {
    const travellerError = this.state.travellerDetails.map((traveller) => {
      const error = {};
      if (isEmpty(traveller.firstName)) {
        error.firstName = "Firstname is required";
      }
      if (isEmpty(traveller.lastName)) {
        error.lastName = "Lastname is required";
      }
      if (isEmpty(traveller.gender)) {
        error.gender = "Gender is required";
      }

      return error;
    });

    let generic;
    if (
      this.state.travellerDetails.length !==
      this.state.bookingDetails.num_passengers
    ) {
      generic = "All traveller details are required";
    }

    const travellerFilled = travellerError.reduce((acc, current) => {
      if (!isEmpty(current)) {
        acc = false;
      }

      return acc;
    }, true);

    this.setState(
      {
        errors: {
          travellerError,
          generic,
        },
      },
      () => {
        if (isEmpty(generic) && travellerFilled) {
          this.submitTravellers();
        }
      }
    );
  };

  submitTravellers = () => {
    const { travellerDetails } = this.state;

    server
      .post("/flights/booking/ticket", {
        bookingId: this.state.bookingDetails.id,
        passengers: travellerDetails,
      })
      .then((res) => {
        this.props.history.push(
          `/flights/select-seats?bookingId=${this.state.bookingDetails.id}`
        );
      })
      .catch((error) => {
        this.setState({
          errors: error || error.response.data,
        });
      });
  };

  render() {
    const { classes } = this.props;
    const { flightDetails, bookingDetails } = this.state;
    return (
      <div>
        <Header user={this.props.user} logout={this.props.logout} />
        <Card className={classes.heading}>
          <Container maxWidth="lg">
            <Typography variant="h5">TRAVELLER &amp; SUMMARY</Typography>
          </Container>
        </Card>
        {this.state.status === "loaded" ? (
          <Container maxWidth="lg">
            <Grid container spacing={3}>
              <Grid item xs lg={8}>
                <Typography className={classes.cardTitle} variant="h6">
                  Flight Details
                </Typography>
                <Card className={classes.flightDetails}>
                  <Card className={classes.shortInfo} elevation={0}>
                    <Typography>
                      {flightDetails.fromAirport.iata.toUpperCase()}-
                      {flightDetails.toAirport.iata.toUpperCase()}
                    </Typography>
                    <Typography
                      variant="subtitle1"
                      className={classes.subtitle}
                    >
                      Non stop | {flightDetails.duration} | Economy
                    </Typography>
                  </Card>
                  <FlightItem data={flightDetails} noAction />
                  <Card className={classes.shortInfo} elevation={0}>
                    <Grid
                      container
                      direction="row"
                      justifyContent="flex-start"
                      alignItems="center"
                      spacing={4}
                    >
                      <Grid item>
                        <Typography className={classes.subtitle}>
                          <strong>BAGGAGE :</strong>
                        </Typography>
                        <Typography
                          variant="subtitle1"
                          className={classes.subtitle}
                        >
                          ADULT
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography className={classes.subtitle}>
                          <strong>CHECK IN</strong>
                        </Typography>
                        <Typography
                          variant="subtitle1"
                          className={classes.subtitle}
                        >
                          15 Kgs (1 piece only)
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography className={classes.subtitle}>
                          <strong>CABIN</strong>
                        </Typography>
                        <Typography
                          variant="subtitle1"
                          className={classes.subtitle}
                        >
                          7 Kgs (1 piece only)
                        </Typography>
                      </Grid>
                    </Grid>
                  </Card>
                </Card>
              </Grid>
              <Grid item xs lg={4}>
                <Typography className={classes.cardTitle} variant="h6">
                  object Fare Summary
                </Typography>
                <Card className={classes.fareDetails}>
                  <div>
                    <Typography>
                      <strong>Base Fare</strong>
                    </Typography>
                    <Grid container justifyContent="space-between">
                      <Grid item>
                        <Typography
                          variant="subtitle1"
                          className={classes.subtitle}
                        >
                          Adult(s) ({bookingDetails.num_passengers} X ₹{" "}
                          {bookingDetails.base_price})
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography>
                          <strong>₹ {bookingDetails.total}</strong>
                        </Typography>
                      </Grid>
                    </Grid>
                  </div>
                  <div>
                    <Grid container justifyContent="space-between">
                      <Grid item>
                        <Typography>
                          <strong>Free &amp; Subcharges</strong>
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography>
                          <strong>₹ {bookingDetails.tax}</strong>
                        </Typography>
                      </Grid>
                    </Grid>
                  </div>
                  <div>
                    <Grid container justifyContent="space-between">
                      <Grid item>
                        <Typography>
                          <strong>Total Ammount:</strong>
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography>
                          <strong>₹ ₹ {bookingDetails.payable_with_tax}</strong>
                        </Typography>
                      </Grid>
                    </Grid>
                  </div>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={this.onContinue}
                    className={classes.continueButton}
                  >
                    Continue
                  </Button>
                  {this.state.errors.message ? (
                    <FormHelperText className={classes.error}>
                      jkahjsf
                    </FormHelperText>
                  ) : null}
                </Card>
              </Grid>
            </Grid>

            <Grid container spacing={3}>
              <Grid item xs lg={8}>
                <Typography variant="h6">Traveller Details</Typography>
                <Card className={classes.flightDetails}>
                  <Card className={classes.shortInfo} elevation={0}>
                    <Grid container justifyContent="space-between">
                      <Grid item>
                        <Typography>
                          <strong>Add Passengers details below</strong>
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography>
                          <strong>
                            {this.state.travellers}/
                            {this.state.bookingDetails.num_passengers} Selected
                          </strong>
                        </Typography>
                      </Grid>
                    </Grid>
                    <FormHelperText className={classes.error}>
                      {this.state.errors.generic}
                    </FormHelperText>
                    {(() => {
                      const array = [];
                      for (
                        let index = 0;
                        index < this.state.travellers;
                        index++
                      ) {
                        array.push(
                          <PassengerDetailForm
                            key={index}
                            passengerNumber={index + 1}
                            handleChange={this.handleChange(index)}
                            data={this.state.travellerDetails[index]}
                            errors={this.state.errors.travellerError[index]}
                          />
                        );
                      }
                      return array;
                    })()}
                  </Card>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={this.addMoreTraveller}
                    className={classes.addMoreButton}
                  >
                    Add more
                  </Button>
                </Card>
              </Grid>
            </Grid>
          </Container>
        ) : null}
        {this.state.status === "loading" ? <Loader /> : null}
        {this.state.status === "failed" ? (
          <Card className={classes.notFound}>
            <Typography variant="h4">{this.state.errors.message}</Typography>
          </Card>
        ) : null}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Review);
