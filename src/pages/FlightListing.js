import { Card, Container, Typography, withStyles } from "@material-ui/core";
import React, { Component } from "react";

import Header from "../components/Header";
import FlightSearch from "../components/FilghtSearch";
import Filter from "../components/Filter";
import FlightSearchResult from "../components/FlightSearchResult";
import server from "../api/server";
import Loader from "../components/Loader";
import queryString from "query-string";

const styles = {
  root: {
    height: 600,
  },
  searchWrapper: {
    display: "flex",
    gap: "2rem",
  },
  notFound: {
    padding: "1rem",
    textAlign: "center",
  },
};

class FlightListing extends Component {
  state = {
    flights: [],
    status: "loading",
    filteredData: [],
    applyFilters: false,
    sortBy: null,
  };

  getFlights = () => {
    server
      .get(`/flights${this.props.location.search}`)
      .then((res) => {
        if (res.data.length > 0) {
          this.setState({
            flights: res.data,
            status: "loaded",
          });
        } else {
          this.setState({
            status: "failed",
          });
        }
      })
      .catch((error) => {
        this.setState({
          status: "failed",
        });
      });
  };

  componentDidMount() {
    this.getFlights();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) {
      this.getFlights();
    }
  }

  filterFlights = (filters) => {
    const toFilter = Object.keys(filters).filter((id) => filters[id] === true);
    const filterd = this.state.flights.filter((flight) =>
      toFilter.includes(flight.airline_id.toString())
    );

    this.setState({
      filteredData: filterd,
      applyFilters: toFilter.length !== 0,
    });
  };

  setSortType = (value) => {
    this.setState({
      sortBy: value,
    });
  };

  render() {
    const { classes } = this.props;
    const { date } = queryString.parse(this.props.location.search);

    const data = this.state.applyFilters
      ? this.state.filteredData
      : this.state.flights;
    return (
      <div className={classes.root}>
        <Header user={this.props.user} logout={this.props.logout} />
        <Container maxWidth="lg">
          {this.state.status === "loading" ? <Loader /> : null}

          {this.state.status === "loaded" || this.state.status === "failed" ? (
            <FlightSearch noIcon />
          ) : null}
          <div className={classes.searchWrapper}>
            {this.state.status === "loaded" ? (
              <>
                <Filter
                  flights={this.state.flights}
                  filterFlights={this.filterFlights}
                />

                <FlightSearchResult
                  flights={data}
                  date={date}
                  setSortType={this.setSortType}
                  user={this.props.user}
                  verifyUser={this.props.verifyUser}
                />
              </>
            ) : null}
          </div>
          {this.state.status === "failed" ? (
            <Card className={classes.notFound}>
              <Typography variant="h4">No Flights Found</Typography>
            </Card>
          ) : null}
        </Container>
      </div>
    );
  }
}

export default withStyles(styles)(FlightListing);
