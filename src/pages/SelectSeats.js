import {
  Box,
  Button,
  Card,
  Container,
  FormHelperText,
  Grid,
  Paper,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import server from "../api/server";
import Header from "../components/Header";
import Loader from "../components/Loader";
import queryString from "query-string";
import PaymentDialogue from "../components/PaymentDialogue";

const styles = (theme) => ({
  heading: {
    background: "#0a223d",
    color: "#fff",
    padding: "1rem",
    borderRadius: 0,
  },
  seat: {
    width: " 50px",
    height: "50px",
    backgroundColor: theme.palette.primary.main,
    cursor: "pointer",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontWeight: 700,
    color: "#fff",
  },
  planeWrapper: {
    padding: "2rem",
    display: "flex",
    backgroundColor: "#ccc",
    width: "2000px",
  },
  seatsWrapper: {
    padding: "1rem",
    flex: 1,
    border: "1px solid #ccc",
    display: "flex",
    flexDirection: "column",
    gap: "1rem",
    justifyContent: "space-between",
  },
  sides: {
    display: "flex",
    flexDirection: "column",
    gap: "1rem",
  },
  makeScroll: {
    overflow: "auto",
  },
  selected: {
    backgroundColor: "#7ed321",
  },
  notAvailable: {
    backgroundColor: "#ccc",
    cursor: "not-allowed",
    pointerEvents: "none",
  },
  info: {
    position: "fixed",
    bottom: 0,
    width: "100%",
  },
  root: {
    minHeight: "100vh",
  },
  border: {
    padding: "0 1rem",
    borderRight: "1px solid #ccc",
  },
  whiteText: {
    color: "#fff",
  },
  seatsInfo: {
    display: "flex",
    gap: "1rem",

    "& > div": {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  },
  notFound: {
    padding: "1rem",
    textAlign: "center",
  },
  error: {
    color: "#f44336",
    padding: "1rem",
    fontSize: "1.5rem",
  },
});

class SelectSeats extends Component {
  state = {
    status: "loading",
    flightDetails: {},
    bookingDetails: {},
    seatsRow: [],
    selectedSeats: {},
    paymentStatus: "",
  };
  componentDidMount() {
    const { bookingId } = queryString.parse(this.props.location.search);
    const accessToken = localStorage.getItem("accessToken");
    const bookingData = server.get(`/flights/booking/${bookingId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const seatsData = server.get(`/flights/booking/seat/${bookingId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });

    Promise.all([bookingData, seatsData])
      .then((pageData) => {
        const seats = pageData[1].data.allSeats;
        const n = 11; //tweak this to add more items per line

        const rows = new Array(Math.ceil(seats.length / n))
          .fill()
          .map((_) => seats.splice(0, n));
        this.setState({
          flightDetails: pageData[0].data.flightDetails,
          bookingDetails: pageData[0].data.getBooking,
          seatsRow: rows,
          status: "loaded",
        });
      })
      .catch((error) => {
        this.setState({
          errors: error || error.response.data,
          status: "failed",
        });
      });
  }

  handleSeatClick = (seatNum, action) => {
    this.setState((state) => {
      const selected = Object.keys(state.selectedSeats).filter(
        (seatNum) => state.selectedSeats[seatNum] === true
      );

      if (
        selected.length === this.state.bookingDetails.num_passengers &&
        action === "add"
      ) {
        return null;
      } else {
        return {
          selectedSeats: {
            ...state.selectedSeats,
            [seatNum]: !state.selectedSeats[seatNum],
          },
        };
      }
    });
  };

  renderRows = (index) => {
    const { classes } = this.props;
    const { seatsRow } = this.state;
    return seatsRow[index].map((seat) => {
      seat.selected = this.state.selectedSeats[seat.seatNum];
      if (seat.isconfirm) {
        return (
          <Grid
            item
            key={seat.seatNum}
            className={`${classes.seat} ${classes.notAvailable}`}
          ></Grid>
        );
      }

      if (seat.selected) {
        return (
          <Grid
            item
            key={seat.seatNum}
            className={`${classes.seat} ${classes.selected}`}
            onClick={() => this.handleSeatClick(seat.seatNum, "remove")}
          >
            {seat.seatNum}
          </Grid>
        );
      }

      return (
        <Grid
          item
          key={seat.seatNum}
          className={classes.seat}
          onClick={() => this.handleSeatClick(seat.seatNum, "add")}
        >
          {seat.seatNum}
        </Grid>
      );
    });
  };

  onPaymentClick = () => {
    const selected = Object.keys(this.state.selectedSeats).filter(
      (seatNum) => this.state.selectedSeats[seatNum] === true
    );

    if (selected.length !== this.state.bookingDetails.num_passengers) {
      return this.setState({
        genericError: "Please select the seats",
      });
    }

    this.setState({
      genericError: "",
    });

    this.showRazorPay();
  };

  showRazorPay = async () => {
    await this.loadRazorPay();

    const { data } = await server.post(
      `/flights/booking/payment/${this.state.bookingDetails.id}`
    );

    const successPayment = this.successPayment;

    const options = {
      key: process.env.REACT_APP_RAZORPAY_KEY_ID,
      currency: data.currency,
      amount: data.amount.toString(),
      order_id: data.id,
      name: "MakeMyTrip",
      description: "Happy Journey",
      image: "https://mmt-clone.netlify.app/favicon.ico",
      handler: successPayment,
    };
    const paymentObject = new window.Razorpay(options);

    paymentObject.on("payment.failed", this.failedPayment);

    paymentObject.open();
  };

  successPayment = (response) => {
    console.log(response.razorpay_payment_id);
    console.log(response.razorpay_order_id);
    console.log(response.razorpay_signature);

    console.log(this);

    this.setState({
      paymentStatus: "success",
    });
  };

  failedPayment = (response) => {
    console.log("Failed", response);

    this.setState({
      genericError: "Payment failed, Please try again ",
    });
  };

  loadRazorPay = () => {
    return new Promise((resolve, reject) => {
      const script = document.createElement("script");
      script.src = "https://checkout.razorpay.com/v1/checkout.js";
      script.onload = () => {
        resolve();
      };
      script.onerror = () => {
        reject();
      };
      document.body.appendChild(script);
    });
  };

  handleDialogueClose = () => {
    this.props.history.push("/");
  };

  render() {
    const { classes } = this.props;
    const { flightDetails, bookingDetails } = this.state;
    const selected = Object.keys(this.state.selectedSeats).filter(
      (seatNum) => this.state.selectedSeats[seatNum] === true
    );
    return (
      <div className={classes.root}>
        <Header user={this.props.user} logout={this.props.logout} />
        {this.state.status === "loading" ? <Loader /> : null}
        {this.state.status === "loaded" ? (
          <Container maxWidth="lg">
            <Card className={classes.heading}>
              <Container maxWidth="lg">
                <Grid
                  container
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Grid item>
                    <Typography variant="h5">Select Seats</Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="h5">
                      {flightDetails.fromAirport.iata}-
                      {flightDetails.toAirport.iata}
                    </Typography>
                  </Grid>
                </Grid>
              </Container>
            </Card>
          </Container>
        ) : null}

        {this.state.status === "loaded" ? (
          <>
            <Box className={classes.makeScroll}>
              <Paper className={classes.planeWrapper}>
                <img
                  className={classes.planeImg}
                  src="/plane-front.png"
                  alt="planeFront"
                />
                <Paper elevation={0} className={classes.seatsWrapper}>
                  <Box className={classes.sides}>
                    <Grid
                      container
                      className={classes.seatsInnerWrapper}
                      justifyContent="space-around"
                    >
                      {this.renderRows(0)}
                    </Grid>
                    <Grid
                      container
                      className={classes.seatsInnerWrapper}
                      justifyContent="space-around"
                    >
                      {this.renderRows(1)}
                    </Grid>
                  </Box>
                  <Box className={classes.sides}>
                    <Grid
                      container
                      className={classes.seatsInnerWrapper}
                      justifyContent="space-around"
                    >
                      {this.renderRows(2)}
                    </Grid>
                    <Grid
                      container
                      className={classes.seatsInnerWrapper}
                      justifyContent="space-around"
                    >
                      {this.renderRows(3)}
                    </Grid>
                  </Box>
                </Paper>
                <img
                  className={classes.planeImg}
                  src="/plane-back.png"
                  alt="planeBack"
                />
              </Paper>
            </Box>
            <FormHelperText className={classes.error}>
              {this.state.genericError}
            </FormHelperText>
            <Card className={`${classes.heading} ${classes.info}`}>
              <Container maxWidth="lg">
                <Grid
                  container
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Grid item>
                    <Grid container>
                      <Grid item className={classes.border}>
                        <Typography>
                          <strong>Select Seat(s)</strong>
                        </Typography>
                        <Typography>
                          {selected.length} of {bookingDetails.num_passengers}{" "}
                          selected
                        </Typography>
                      </Grid>
                      <Grid item className={classes.border}>
                        <Typography>
                          <strong>Total fare</strong>
                        </Typography>
                        <Typography>
                          ₹ {bookingDetails.payable_with_tax}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <Box className={classes.seatsInfo}>
                      <Box>
                        <Box className={classes.seat}></Box>
                        <FormHelperText className={classes.whiteText}>
                          Available
                        </FormHelperText>
                      </Box>
                      <Box>
                        <Box
                          className={`${classes.seat} ${classes.notAvailable}`}
                        ></Box>
                        <FormHelperText className={classes.whiteText}>
                          Not Available
                        </FormHelperText>
                      </Box>
                      <Box>
                        <Box
                          className={`${classes.seat} ${classes.selected}`}
                        ></Box>
                        <FormHelperText className={classes.whiteText}>
                          Selected
                        </FormHelperText>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.onPaymentClick}
                    >
                      Make Payment
                    </Button>
                  </Grid>
                </Grid>
              </Container>
            </Card>
          </>
        ) : null}

        <PaymentDialogue
          open={this.state.paymentStatus === "success" ? true : false}
          handleClose={this.handleDialogueClose}
        />

        {this.state.status === "failed" ? (
          <Card className={classes.notFound}>
            <Typography variant="h4">{this.state.errors.message}</Typography>
          </Card>
        ) : null}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SelectSeats);
