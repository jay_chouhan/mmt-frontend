import { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { CssBaseline, ThemeProvider } from "@material-ui/core";
import theme from "./utils/theme";

import server from "./api/server";

import Home from "./pages/Home";
import FlightListing from "./pages/FlightListing";
import Review from "./pages/Review";
import SelectSeats from "./pages/SelectSeats";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };
  }

  verifyUser = (accessToken) => {
    server
      .get("/users/verify", {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        server.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${accessToken}`;
        this.setState({
          user: res.data,
        });
      })
      .catch((error) => {
        localStorage.removeItem("accessToken");
      });
  };

  logout = () => {
    localStorage.removeItem("accessToken");

    this.setState({
      user: null,
    });
  };

  componentDidMount() {
    const accessToken = localStorage.getItem("accessToken");

    if (accessToken) {
      this.verifyUser(accessToken);
    }
  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route
              path="/"
              exact
              render={() => (
                <Home
                  user={this.state.user}
                  verifyUser={this.verifyUser}
                  logout={this.logout}
                />
              )}
            />

            <Route
              path="/flights"
              exact
              render={(params) => (
                <FlightListing
                  user={this.state.user}
                  {...params}
                  verifyUser={this.verifyUser}
                  logout={this.logout}
                />
              )}
            />

            <Route
              path="/flights/review"
              exact
              render={(params) => (
                <Review
                  user={this.state.user}
                  {...params}
                  verifyUser={this.verifyUser}
                  logout={this.logout}
                />
              )}
            />

            <Route
              path="/flights/select-seats"
              exact
              render={(params) => (
                <SelectSeats
                  user={this.state.user}
                  {...params}
                  verifyUser={this.verifyUser}
                  logout={this.logout}
                />
              )}
            />
          </Switch>
        </Router>
        <CssBaseline />
      </ThemeProvider>
    );
  }
}

export default App;
