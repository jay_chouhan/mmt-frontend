import axios from "axios";

export default axios.create({
  baseURL: "https://make-my-trip-api.herokuapp.com/api",
});
