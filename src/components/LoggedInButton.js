import { Button, Menu, MenuItem } from "@material-ui/core";
import React, { Component } from "react";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

class LoggedInButton extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  onLogoutClick = () => {
    this.props.logout();
    this.handleClose();
  };

  render() {
    return (
      <div>
        <Button
          color="inherit"
          startIcon={<AccountCircleIcon />}
          endIcon={<ArrowDropDownIcon />}
          onClick={this.handleClick}
        >
          {this.props.user.name}
        </Button>

        <Menu
          anchorEl={this.state.anchorEl}
          keepMounted
          open={Boolean(this.state.anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.onLogoutClick}>Logout</MenuItem>
        </Menu>
      </div>
    );
  }
}

export default LoggedInButton;
