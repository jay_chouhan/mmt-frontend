import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@material-ui/core";

import { Component } from "react";
import { withStyles } from "@material-ui/styles";

import PropTypes from "prop-types";
import validator from "./validations/loginValidator";
import server from "../api/server";
// import Signup from "./Signup";

const styles = {
  dialogInput: {
    margin: "5px auto",
  },
  dialogActions: {
    padding: "8px 0",
  },
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      isLoggedIn: false,
      loginErr: false,

      email: "",
      password: "",

      errEmail: {
        msg: "",
        err: false,
      },
      errPassword: {
        msg: "",
        err: false,
      },
    };
  }

  handleInput = (name) => {
    return (event) => {
      this.setState({
        [name]: event.target.value,
      });
    };
  };

  toggleModal = () => {
    this.setState((prevState) => {
      return {
        open: !prevState.open,
      };
    });
  };

  // handleClickOpen = () => {
  //   this.setState({ open: true });
  // };

  // handleClose = () => {
  //   this.setState({ open: false });
  // };

  // handleEmailChange = (e) => {
  //   this.setState({ email: e.target.value });
  // };

  // handlePasswordChange = (e) => {
  //   this.setState({ password: e.target.value });
  // };

  handleLogin = async () => {
    const validation = validator(this.state);
    const { error, isValid } = validation;

    if (isValid === true) {
      this.setState({
        errEmail: {
          msg: "",
          err: false,
        },

        errPassword: {
          msg: "",
          err: false,
        },
      });

      try {
        const res = await server.post("/users/login", {
          email: this.state.email,
          password: this.state.password,
        });

        const { accessToken } = res.data;

        localStorage.setItem("accessToken", `${accessToken}`);

        this.setState({
          isLoggedIn: true,
          loginErr: false,
        });
        this.toggleModal();
        console.log("1");
        this.props.verifyUser(accessToken);
        console.log("2");
      } catch (error) {
        console.log(error);
        this.setState({
          loginErr: true,

          errEmail: {
            err: true,
            msg: "Email and password do not match",
          },

          errPassword: {
            err: true,
            msg: "Email and password do not match",
          },
        });
      }
    } else {
      this.setState({
        errEmail: {
          msg: error.email || "",
          err: error.email !== undefined,
        },
        errPassword: {
          msg: error.password || "",
          err: error.password !== undefined,
        },
      });

      // if (error.email !== undefined) {
      //   this.setState({
      //     errEmail: {
      //       msg: error.email,
      //       err: true,
      //     },
      //   });
      // } else if (error.email === undefined) {
      //   this.setState({
      //     errEmail: {
      //       msg: "",
      //       err: false,
      //     },
      //   });
      // }

      // if (error.password !== undefined) {
      //   this.setState({
      //     errPassword: {
      //       msg: error.password,
      //       err: true,
      //     },
      //   });
      // } else if (error.password === undefined) {
      //   this.setState({
      //     errPassword: {
      //       msg: "",
      //       err: false,
      //     },
      //   });
      // }
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button color="inherit" onClick={this.toggleModal}>
          {this.props.text ? this.props.text : "Login"}
        </Button>

        <Dialog
          open={this.state.open}
          onClose={this.toggleModal}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle>Login</DialogTitle>

          <DialogContent className={classes.dialog}>
            {!this.state.errEmail.err ? (
              <TextField
                required
                onInput={this.handleInput("email")}
                id="outlined-required"
                label="Email"
                variant="outlined"
                className={classes.dialogInput}
                fullWidth
              />
            ) : (
              <TextField
                error
                required
                onInput={this.handleInput("email")}
                id="outlined-required"
                label="Email"
                variant="outlined"
                className={classes.dialogInput}
                helperText={this.state.errEmail.msg}
                fullWidth
              />
            )}
            {!this.state.errPassword.err ? (
              <TextField
                required
                id="outlined-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                variant="outlined"
                onInput={this.handleInput("password")}
                className={classes.dialogInput}
                fullWidth
              />
            ) : (
              <TextField
                error
                required
                id="outlined-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                variant="outlined"
                onInput={this.handleInput("password")}
                className={classes.dialogInput}
                helperText={this.state.errPassword.msg}
                fullWidth
              />
            )}

            <DialogActions className={classes.dialogActions}>
              <Button
                onClick={this.handleLogin}
                variant="contained"
                color="primary"
                fullWidth
              >
                Login
              </Button>
            </DialogActions>
            {/* <Signup onClick={this.toggleModal} /> */}
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
