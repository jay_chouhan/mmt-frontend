import { Card, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import FlightTakeoffOutlinedIcon from "@material-ui/icons/FlightTakeoffOutlined";
import TrainOutlinedIcon from "@material-ui/icons/TrainOutlined";
import DriveEtaOutlinedIcon from "@material-ui/icons/DriveEtaOutlined";
import DirectionsBusOutlinedIcon from "@material-ui/icons/DirectionsBusOutlined";
import HomeWorkOutlinedIcon from "@material-ui/icons/HomeWorkOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0.5rem 1.5rem",
    display: "flex",
    position: "absolute",
    top: 0,
    left: "50%",
    transform: "translate(-50%,-50%)",
    justifyContent: "center",
    gap: "3rem",
    boxShadow: "0px 2px 5px #777",

    [theme.breakpoints.down("sm")]: {
      gap: 0,
      justifyContent: "space-between",
      width: "95%",
      transform: "translate(-50%,100%)",
    },
  },
  icon: {
    fontSize: "2rem",
    color: "#777",
  },
  label: {
    textAlign: "center",
    fontSize: "0.9rem",
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
  },
}));

const IconBar = () => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <div>
        <FlightTakeoffOutlinedIcon
          color="primary"
          className={`${classes.icon} ${classes.active}`}
        />
        <Typography className={`${classes.label} ${classes.active}`}>
          Flights
        </Typography>
      </div>
      <div>
        <TrainOutlinedIcon className={classes.icon} />
        <Typography className={classes.label}>Trains</Typography>
      </div>
      <div>
        <DriveEtaOutlinedIcon className={classes.icon} />
        <Typography className={classes.label}>Cabs</Typography>
      </div>
      <div>
        <DirectionsBusOutlinedIcon className={classes.icon} />
        <Typography className={classes.label}>Bus</Typography>
      </div>
      <div>
        <HomeWorkOutlinedIcon className={classes.icon} />
        <Typography className={classes.label}>Hotels</Typography>
      </div>
    </Card>
  );
};

export default IconBar;
