import { Card, makeStyles, Typography } from "@material-ui/core";
import React from "react";
// import ArrowDropUpTwoToneIcon from "@material-ui/icons/ArrowDropUpTwoTone";
import FlightItem from "./FlightItem";
import moment from "moment";

const useStyles = makeStyles({
  root: {
    flex: 1,
  },
  sortWrapper: {
    display: "flex",
    alignItems: "center",
    padding: "0.5rem",
    margin: "1rem",
    backgroundColor: "#f4f4f4",
    fontSize: "0.9rem",

    "& div": {
      flex: 1,
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
    },
  },
  heading: {
    padding: "1rem",
  },
});

const FlightSearchResult = (props) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <Typography variant="h6" className={classes.heading}>
        Showing Flights for {moment(props.date).format("MMM Do YY")}
      </Typography>
      {/* <Card spacing={3} className={classes.sortWrapper}>
        <div>
          <strong>Sorted By:</strong>
        </div>
        <div onClick={() => props.setSortType("departure")}>Departure</div>
        <div onClick={() => props.setSortType("duration")}>Duration</div>
        <div onClick={() => props.setSortType("arrival")}>Arrival</div>
        <div onClick={() => props.setSortType("price")}>Price</div>
        <div></div>
      </Card> */}
      <div className={classes.flightsWrapper}>
        {props.flights.map((flight) => (
          <FlightItem
            data={flight}
            key={flight.id}
            user={props.user}
            verifyUser={props.verifyUser}
          />
        ))}
      </div>
    </Card>
  );
};

export default FlightSearchResult;
