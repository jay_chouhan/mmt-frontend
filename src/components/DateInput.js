import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import React, { Component } from "react";
import { withStyles } from "@material-ui/core";

import moment from "moment";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
  dateWrapper: {
    width: "150px",

    [theme.breakpoints.down("sm")]: {
      width: "100%",

      "& .MuiInputLabel-root": {
        padding: "0.5rem",
      },
    },
  },
});

class DateInput extends Component {
  render() {
    const { classes } = this.props;
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} className={classes.root}>
        <DatePicker
          autoOk
          label={this.props.label}
          clearable
          className={classes.dateWrapper}
          disablePast
          value={this.props.value}
          helperText={moment(this.props.value).format("dddd")}
          onChange={(date) => this.props.handleInputs(this.props.id, date)}
        />
      </MuiPickersUtilsProvider>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DateInput);
