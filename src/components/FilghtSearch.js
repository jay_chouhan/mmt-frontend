import {
  Button,
  Card,
  FormControlLabel,
  Radio,
  RadioGroup,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";

import AirportInput from "./AirportInput";
import DateInput from "./DateInput";
import TravellersAndClass from "./TravellersAndClass";
import IconBar from "./IconBar";
import server from "../api/server";
import { withRouter } from "react-router-dom";
import moment from "moment";

const styles = (theme) => ({
  root: {
    width: "100%",
    margin: "5rem auto",
    padding: "5rem 2rem 3rem 2rem",
    position: "relative",
    overflow: "initial",
    boxShadow: "0 5px 10px #15457c",
    zIndex: 1,

    [theme.breakpoints.down("sm")]: {
      padding: "0",
      position: "initial",
    },
  },
  tripTypeWrapper: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",

    [theme.breakpoints.down("sm")]: {
      paddingTop: "1rem",
      paddingLeft: "0.5rem",
    },
  },
  info: {
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "0.5rem",
    },
  },
  inputWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    gap: "1rem",
    border: "1px solid #ccc",
    borderRadius: "10px",
    padding: "0.5rem 1rem",

    [theme.breakpoints.down("sm")]: {
      border: "none",
      flexDirection: "column",
      alignItems: "flex-start",
    },

    "& > *": {
      [theme.breakpoints.down("sm")]: {
        border: "1px solid rgb(231, 231, 231)",
        backgroundColor: "rgb(247, 247, 247)",
        padding: "0.5rem",
        width: "100%",
      },
    },
  },
  searchButton: {
    position: "absolute",
    bottom: "0",
    left: "50%",
    transform: "translate(-50%,50%)",
    zIndex: "1",
    padding: "0.5rem 1rem",
    backgroundImage: "linear-gradient(to right,#53b2fe,#065af3)",
    color: "#fff",
    borderRadius: "50px",
    fontWeight: "bold",
    fontSize: "1rem",
  },
  noIcon: {
    margin: "3rem auto",
    padding: "2rem",
    width: "initial",
  },
});

class FilghtSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tripType: "oneway",
      airports: [],
      from: null,
      to: null,
      departure: new Date(),
      arrival: moment().add(1, "days"),
      noOfTravellers: 1,
    };
  }

  componentDidMount() {
    server
      .get("/airports")
      .then((res) => {
        this.setState({
          airports: res.data,
          from: res.data[0],
          to: res.data[1],
        });
      })
      .catch((error) => console.log(error));
  }

  handleInputs = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  handleTripChange = (event) => {
    this.setState({
      tripType: event.target.value,
    });
  };

  onSearch = () => {
    const { tripType, from, to, noOfTravellers } = this.state;
    const departure = moment(this.state.departure).format("YYYY-MM-DD");
    this.props.history.push(
      `/flights?tripType=${tripType}&from=${from.id}&to=${to.id}&date=${departure}&seats=${noOfTravellers}&departure=${this.state.departure}`
    );
  };

  render() {
    const { classes } = this.props;
    const { tripType } = this.state;
    return (
      <Card
        className={
          this.props.noIcon
            ? `${classes.root} ${classes.noIcon}`
            : `${classes.root}`
        }
      >
        {!this.props.noIcon ? <IconBar /> : null}

        <RadioGroup
          className={classes.tripTypeWrapper}
          aria-label="gender"
          name="gender1"
          value={tripType}
          onChange={this.handleTripChange}
        >
          <FormControlLabel
            value="oneway"
            control={<Radio color="primary" />}
            label="ONEWAY"
          />
          <FormControlLabel
            value="roundtrip"
            control={<Radio color="primary" />}
            label="ROUND TRIP"
          />
        </RadioGroup>
        <Typography className={classes.info}>
          Book Domestic and International flights
        </Typography>

        <div className={classes.inputWrapper}>
          <AirportInput
            label="FROM"
            id="from"
            airports={this.state.airports}
            handleAirportInput={this.handleInputs}
            value={this.state.from}
          />
          <AirportInput
            label="TO"
            id="to"
            airports={this.state.airports}
            handleAirportInput={this.handleInputs}
            value={this.state.to}
          />
          <DateInput
            label="DEPARTURE"
            handleInputs={this.handleInputs}
            value={this.state.departure}
            id="departure"
          />
          {this.state.tripType === "roundtrip" ? (
            <DateInput
              label="RETURN"
              id="return"
              handleInputs={this.handleInputs}
              value={this.state.arrival}
            />
          ) : null}
          <TravellersAndClass
            id="noOfTravellers"
            handleInputs={this.handleInputs}
            value={this.state.noOfTravellers}
          />
        </div>

        <Button className={classes.searchButton} onClick={this.onSearch}>
          Search Flights
        </Button>
      </Card>
    );
  }
}

export default withRouter(
  withStyles(styles, { withTheme: true })(FilghtSearch)
);
