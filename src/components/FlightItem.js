import {
  Button,
  Card,
  FormHelperText,
  Typography,
  withStyles,
} from "@material-ui/core";
import moment from "moment";
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Login from "./Login";
import isEmpty from "./utils/isEmpty";
import queryString from "query-string";
import server from "../api/server";

const styles = (theme) => ({
  cardItem: {
    display: "flex",
    alignItems: "center",
    padding: "2rem 0.5rem",
    margin: "1rem",
    fontSize: "0.9rem",
    borderBottom: "1px solid #ccc",
    boxShadow: "none",

    "& div": {
      flex: 1,
      display: "flex",
      alignItems: "center",
    },
    "& p": {
      flex: 1,
      display: "flex",
      alignItems: "center",
      textAlign: "center",
    },
  },
  airline: {
    gap: "1rem",
    "& img": {
      width: 32,
    },
    "& p": {
      fontWeight: "bold",
    },
  },
  column: {
    flexDirection: "column",
    alignItems: "flex-start!important",
  },
  bookNowButton: {
    backgroundImage: "linear-gradient(to right,#53b2fe,#065af3)",
    color: "#fff",
    borderRadius: "50px",
    fontWeight: "bold",
    padding: "0.3rem 1rem",
  },
  small: {
    fontSize: "0.9rem",
    fontWeight: "300",

    "& p": {
      fontSize: "0.9rem",
      fontWeight: "300",
    },
  },
  duration: {
    "& p": {
      fontSize: "0.8rem",
    },
  },
  flightDetailsButton: {
    "& p": {
      fontSize: "0.7rem",
    },
  },
  error: {
    color: "#f44336",
    padding: "1rem",
  },
});

class FlightItem extends Component {
  state = {
    error: "",
  };

  onBookClick = (id) => {
    const { seats } = queryString.parse(this.props.location.search);

    server
      .post("/flights/booking", {
        flightId: id,
        numPassengers: seats,
      })
      .then((res) => {
        this.props.history.push(
          `/flights/review?flightId=${id}&numPassengers=${seats}&bookingId=${res.data.newBooking.id}`
        );
      })
      .catch((error) => {
        console.log(error.response);
        this.setState({ error: error.response.data.msg });
      });
  };

  render() {
    const { classes, data } = this.props;

    const {
      airline,
      arrival_time,
      boarding_time,
      duration,
      fromAirport,
      price_per_seat,
      toAirport,
      id,
    } = data;

    const isLoggedIn = !isEmpty(this.props.user);

    return (
      <Card className={classes.cardItem}>
        <div className={classes.airline}>
          <img src={airline.iconurl} alt="airline-logo" />
          <Typography>{airline.name}</Typography>
        </div>
        <div className={classes.column}>
          <Typography>
            <strong>
              {moment(boarding_time, "HH:mm:ss").format("hh:mm A")}
            </strong>
          </Typography>
          <Typography className={classes.small}>{fromAirport.city}</Typography>
        </div>
        <div
          className={`${classes.column} ${classes.small} ${classes.duration}`}
        >
          <Typography>{duration}</Typography>
          <Typography>Non stop</Typography>
        </div>
        <div className={classes.column}>
          <Typography>
            {moment(arrival_time, "HH:mm:ss").format("hh:mm A")}
          </Typography>
          <Typography className={classes.small}>{toAirport.city}</Typography>
        </div>
        <div>
          <Typography>
            <strong>₹ {price_per_seat}</strong>
          </Typography>
        </div>
        {!this.props.noAction ? (
          <div className={classes.column}>
            {isLoggedIn ? (
              <>
                <Button
                  className={classes.bookNowButton}
                  onClick={() => this.onBookClick(id)}
                >
                  Book Now
                </Button>
                {this.state.error ? (
                  <FormHelperText className={classes.error}>
                    {this.state.error}
                  </FormHelperText>
                ) : null}
              </>
            ) : (
              <Login text="Login to Book" verifyUser={this.props.verifyUser} />
            )}
          </div>
        ) : null}
      </Card>
    );
  }
}

export default withRouter(withStyles(styles, { withTheme: true })(FlightItem));
