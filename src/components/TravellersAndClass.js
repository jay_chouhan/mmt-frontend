import {
  Button,
  ButtonGroup,
  Card,
  Popover,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import ExpandMoreOutlinedIcon from "@material-ui/icons/ExpandMoreOutlined";

import PropTypes from "prop-types";

const styles = (theme) => ({
  typography: {
    padding: "2rem",
  },
  card: {
    height: "70px",
    boxShadow: "none",
    cursor: "pointer",
  },
  popoverCard: {
    padding: "1rem",
  },
  label: {
    display: "flex",
    fontSize: "0.8rem",
    color: "rgba(0, 0, 0, 0.54)",
    justifyContent: "space-between",
  },
  root: {
    height: "100%",
    width: "150px",

    [theme.breakpoints.down("sm")]: {
      width: "100%",
      "& .MuiPaper-root": {
        backgroundColor: "rgb(247, 247, 247)",
      },
    },
  },
});

class TravellersAndClass extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
    };
  }

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const open = Boolean(this.state.anchorEl);
    const id = open ? "simple-popover" : undefined;

    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Card
          aria-describedby={id}
          onClick={this.handleClick}
          className={classes.card}
        >
          <Typography className={classes.label}>
            Travellers
            <ExpandMoreOutlinedIcon />
          </Typography>
          <Typography>
            <strong>{this.props.value}</strong> Traveller
          </Typography>
        </Card>

        <Popover
          id={id}
          open={open}
          anchorEl={this.state.anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <Card className={classes.popoverCard}>
            <Typography>No of Travellers</Typography>
            <ButtonGroup disableElevation aria-label="button group">
              {noArray.map((item) => {
                return (
                  <Button
                    key={item}
                    color={this.props.value === item ? "primary" : null}
                    variant={this.props.value === item ? "contained" : null}
                    onClick={() => this.props.handleInputs(this.props.id, item)}
                  >
                    {item}
                  </Button>
                );
              })}
            </ButtonGroup>
          </Card>
        </Popover>
      </div>
    );
  }
}

const noArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];

TravellersAndClass.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(TravellersAndClass);
