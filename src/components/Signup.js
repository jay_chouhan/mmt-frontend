import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  TextField,
} from "@material-ui/core";
import { Component } from "react";
import { withStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import server from "../api/server";
import validator from "./validations/signupValidator";

const styles = {
  dialogInput: {
    margin: "5px auto",
  },
  dialogActions: {
    padding: "8px 0",
  },
};

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      name: "",
      email: "",
      password: "",
      confPassword: "",
      otpGenerated: false,
      otp: "",
      errName: {
        msg: "",
        err: false,
      },
      errEmail: {
        msg: "",
        err: false,
      },
      errConfPassword: {
        msg: "",
        err: false,
      },
      errPassword: {
        msg: "",
        err: false,
      },
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, otpGenerated: false });
  };

  handleEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };

  handlePasswordChange = (e) => {
    this.setState({ password: e.target.value });
  };

  handleNameChange = (e) => {
    this.setState({ name: e.target.value });
  };

  handleConfPasswordChange = (e) => {
    this.setState({ confPassword: e.target.value });
  };

  handleOtpGeneration = async () => {
    const validation = validator(this.state);
    const { error, isValid } = validation;

    if (isValid === true) {
      this.setState({
        errName: {
          msg: "",
          err: false,
        },
        errEmail: {
          msg: "",
          err: false,
        },
        errConfPassword: {
          msg: "",
          err: false,
        },
        errPassword: {
          msg: "",
          err: false,
        },
      });

      try {
        const res = await server.post("/users/signup", {
          email: this.state.email,
          password: this.state.password,
          name: this.state.name,
          password2: this.state.confPassword,
        });

        const { otpToken } = res.data;

        this.setState({ otpGenerated: true });

        localStorage.setItem("otpToken", `${otpToken}`);
      } catch (error) {
        this.setState({
          errEmail: {
            msg: "Email already exists",
            err: true,
          },
        });
      }
    } else {
      console.log(error);
      if (error.name !== undefined) {
        this.setState({
          errName: {
            msg: error.name,
            err: true,
          },
        });
      } else if (error.name === undefined) {
        this.setState({
          errName: {
            msg: "",
            err: false,
          },
        });
      }

      if (error.email !== undefined) {
        this.setState({
          errEmail: {
            msg: error.email,
            err: true,
          },
        });
      } else if (error.email === undefined) {
        this.setState({
          errEmail: {
            msg: "",
            err: false,
          },
        });
      }

      if (error.password !== undefined) {
        this.setState({
          errPassword: {
            msg: error.password,
            err: true,
          },
        });
      } else if (error.password === undefined) {
        this.setState({
          errPassword: {
            msg: "",
            err: false,
          },
        });
      }

      if (error.confPassword !== undefined) {
        this.setState({
          errConfPassword: {
            msg: error.confPassword,
            err: true,
          },
        });
      } else if (error.confPassword === undefined) {
        this.setState({
          errConfPassword: {
            msg: "",
            err: false,
          },
        });
      }
    }
  };

  handleOtpSignup = async () => {
    try {
      const res = await server.post(
        "/users/signup/verify-otp",
        {
          otp: this.state.otp,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("otpToken")}`,
          },
        }
      );

      const { accessToken } = res.data;

      localStorage.setItem("accessToken", `${accessToken}`);

      this.handleClose();

      this.props.verifyUser(accessToken);
    } catch (error) {
      console.log(error);

      // !No state handling?
      /*
        this.setState({
          genericError: {
            err: true,
            message: error.response && error.response.error || "A server error occurred",
          }
        });

        this.setState((prevState) => {
          return {
            
            genericError: {
              err: true,
              message: error.response && error.response.error || "A server error occurred",
            }
          }
        });
      */
    }
  };

  handleOtpChange = (event) => {
    this.setState({ otp: event.target.value });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button color="inherit" onClick={this.handleClickOpen}>
          Signup
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          {!this.state.otpGenerated ? (
            <>
              <DialogTitle id="form-dialog-title">Signup</DialogTitle>
              <DialogContent className={classes.dialog}>
                {!this.state.errName.err ? (
                  <TextField
                    required
                    label="Name"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleNameChange}
                    fullWidth
                  />
                ) : (
                  <TextField
                    error
                    required
                    label="Name"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleNameChange}
                    helperText={this.state.errName.msg}
                    fullWidth
                  />
                )}

                {!this.state.errEmail.err ? (
                  <TextField
                    required
                    label="Email"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleEmailChange}
                    fullWidth
                  />
                ) : (
                  <TextField
                    error
                    required
                    label="Email"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleEmailChange}
                    helperText={this.state.errEmail.msg}
                    fullWidth
                  />
                )}
                {!this.state.errPassword.err ? (
                  <TextField
                    required
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handlePasswordChange}
                    fullWidth
                  />
                ) : (
                  <TextField
                    error
                    required
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handlePasswordChange}
                    helperText={this.state.errPassword.msg}
                    fullWidth
                  />
                )}
                {!this.state.errConfPassword.err ? (
                  <TextField
                    required
                    label="Confirm Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleConfPasswordChange}
                    fullWidth
                  />
                ) : (
                  <TextField
                    error
                    required
                    label="Confirm Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    className={classes.dialogInput}
                    onInput={this.handleConfPasswordChange}
                    fullWidth
                    helperText={this.state.errConfPassword.msg}
                  />
                )}

                <DialogActions className={classes.dialogActions}>
                  <Button
                    onClick={this.handleOtpGeneration}
                    variant="contained"
                    color="primary"
                    fullWidth
                  >
                    Signup
                  </Button>
                </DialogActions>
              </DialogContent>
            </>
          ) : (
            <>
              <DialogTitle id="form-dialog-title">Enter OTP</DialogTitle>
              <DialogContent className={classes.dialog}>
                <Typography variant="subtitle1">
                  Please Check your email for an otp
                </Typography>
                <TextField
                  required
                  id="outlined-required"
                  label="OTP"
                  variant="outlined"
                  className={classes.dialogInput}
                  onInput={this.handleOtpChange}
                  value={this.state.otp}
                  fullWidth
                />

                <DialogActions className={classes.dialogActions}>
                  <Button
                    onClick={this.handleOtpSignup}
                    variant="contained"
                    color="primary"
                    fullWidth
                  >
                    Verify
                  </Button>
                </DialogActions>
              </DialogContent>
            </>
          )}
        </Dialog>
      </div>
    );
  }
}

Signup.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Signup);
