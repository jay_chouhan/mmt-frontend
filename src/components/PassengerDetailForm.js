import {
  Button,
  ButtonGroup,
  FormHelperText,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles({
  root: {
    padding: "1rem",
    borderBottom: "1px solid #ccc",
  },
  subtitle: {
    fontSize: "0.9rem",
  },
  error: {
    color: "#f44336",
  },
});

const PassengerDetailForm = (props) => {
  const { data } = props;
  const classes = useStyles();

  const errors = props.errors ? props.errors : {};

  return (
    <div className={classes.root}>
      <Typography variant="h6">Adult {props.passengerNumber}</Typography>
      <Typography variant="subtitle1" className={classes.subtitle}>
        <strong>
          IMPORTANT: Enter your name as it is mentioned on your passport or any
          government approved ID.
        </strong>
      </Typography>
      <Grid container justifyContent="space-between" alignItems="center">
        <Grid item lg={4}>
          <TextField
            error={!!errors.firstName}
            label="First name"
            onChange={(e) => props.handleChange("firstName", e.target.value)}
            value={data.firstName}
            helperText={errors.firstName}
          />
        </Grid>
        <Grid item lg={4}>
          <TextField
            error={!!errors.lastName}
            label="Last name"
            onChange={(e) => props.handleChange("lastName", e.target.value)}
            value={data.lastName}
            helperText={errors.lastName}
          />
        </Grid>
        <Grid item lg={4}>
          <ButtonGroup disableElevation aria-label="button group">
            <Button
              color={data.gender === "M" ? "primary" : null}
              variant={data.gender === "M" ? "contained" : null}
              onClick={(e) => props.handleChange("gender", "M")}
            >
              Male
            </Button>
            <Button
              color={data.gender === "F" ? "primary" : null}
              variant={data.gender === "F" ? "contained" : null}
              onClick={(e) => props.handleChange("gender", "F")}
            >
              Female
            </Button>
          </ButtonGroup>
          {errors.gender ? (
            <FormHelperText className={classes.error}>
              {errors.gender}
            </FormHelperText>
          ) : null}
        </Grid>
      </Grid>
    </div>
  );
};

export default PassengerDetailForm;
