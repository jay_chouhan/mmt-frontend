import { makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles({
  loader: {
    "& img": {
      margin: "auto",
      display: "block",
      marginTop: "100px",
      width: "400px",
    },
  },
});

const Loader = () => {
  const classes = useStyles();
  return (
    <div className={classes.loader}>
      <img src="/loader.svg" alt="loader" />
    </div>
  );
};

export default Loader;
