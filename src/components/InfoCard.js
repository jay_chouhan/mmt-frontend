import React from "react";
import { Card, CardContent, makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    padding: "0 1rem",
    flex: 1,
    display: "flex",
    gap: "1rem",
    alignItems: "center",
    boxShadow: "0 5px 10px #ccc",
  },
});

const InfoCard = (props) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      {props.icon}
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        </Typography>
      </CardContent>
    </Card>
  );
};

export default InfoCard;
