import { makeStyles } from "@material-ui/core";
import React from "react";
import BlogItem from "./BlogItem";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-around",
    padding: "2rem",

    [theme.breakpoints.down("sm")]: {
      padding: "1rem",
      flexDirection: "column",
      alignItems: "center",
      gap: "1rem",
    },
  },
}));

const BlogList = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <BlogItem image="https://source.unsplash.com/qyAka7W5uMY/500x500" />
      <BlogItem image="https://source.unsplash.com/oCdVtGFeDC0/500x500" />
      <BlogItem image="https://source.unsplash.com/6Mxb_mZ_Q8E/500x500" />
    </div>
  );
};

export default BlogList;
