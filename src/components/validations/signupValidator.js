import validator from 'validator';
import isEmpty from '../utils/isEmpty';

const signupValidator = (data) => {
  const { name, email, password, confPassword } = data;

  const error = {};
  if (!validator.isEmail(email)) {
    error.email = 'Invalid Email';
  }

  if (isEmpty(email)) {
    error.email = 'email is required';
  }

  if (!validator.isLength(name, { min: 3, max: 50 })) {
    error.name = 'name must be between 4 to 50 characters long';
  }

  if (isEmpty(name)) {
    error.name = 'Name is required';
  }

  if (!validator.isLength(password, { min: 6, max: 30 })) {
    error.password = 'Password must be between 6 to 30 characters long';
  }

  if (isEmpty(password)) {
    error.password = 'Password is required';
  }

  if (!validator.equals(password, confPassword)) {
    error.confPassword = 'Passwords do not match';
  }

  if (isEmpty(confPassword)) {
    error.confPassword = 'Password confirmation is required';
  }

  return { error, isValid: isEmpty(error) };
};

export default signupValidator;
