import validator from 'validator';
import isEmpty from '../utils/isEmpty';

const loginValidator = (data) => {
  const { email, password } = data;

  const error = {};

  if (!validator.isEmail(email)) {
    error.email = 'Invalid Email';
  }

  if (isEmpty(email)) {
    error.email = 'email is required';
  }

  if (!validator.isLength(password, { min: 6, max: 30 })) {
    error.password = 'Password must be between 6 to 30 characters long';
  }

  if (isEmpty(password)) {
    error.password = 'Password is required';
  }

  return { error, isValid: isEmpty(error) };
};

export default loginValidator;
