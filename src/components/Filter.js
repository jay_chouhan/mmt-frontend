import {
  Card,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";

const style = (theme) => ({
  root: {
    width: "300px",
    padding: "1rem",
  },
});

class Filter extends Component {
  state = {};

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.checked }, () => {
      this.props.filterFlights(this.state);
    });
  };
  render() {
    const { classes } = this.props;

    const airlines = this.props.flights.map((flight) => {
      return {
        airline: flight.airline.name,
        id: flight.airline_id,
      };
    });

    const filterLabels = [
      ...new Map(airlines.map((airline) => [airline["id"], airline])).values(),
    ];

    return (
      <Card className={classes.root}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Filter By Flights</FormLabel>
          <FormGroup>
            {filterLabels.map((filterItem) => (
              <FormControlLabel
                key={filterItem.id}
                control={
                  <Checkbox
                    checked={this.state[`${filterItem.id}`] || false}
                    color="primary"
                    onChange={this.handleChange}
                    name={`${filterItem.id}`}
                  />
                }
                label={filterItem.airline}
              />
            ))}
          </FormGroup>
        </FormControl>
      </Card>
    );
  }
}

export default withStyles(style)(Filter);
