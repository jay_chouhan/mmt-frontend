import React, { Component } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { TextField, withStyles } from "@material-ui/core";
import isEmpty from "../components/utils/isEmpty";

const styles = {
  root: {
    width: "300px",
  },
  "input:before": {
    border: "none",
  },
  airport: {
    fontSize: "0.8rem",
    color: "#777",
  },
};

class AirportInput extends Component {
  render() {
    const { classes, airports } = this.props;
    return (
      <Autocomplete
        disableClearable
        className={classes.root}
        options={airports}
        getOptionLabel={(option) => `${option.city}, ${option.country}`}
        value={this.props.value}
        onChange={(event, newValue) => {
          this.props.handleAirportInput(this.props.id, newValue);
        }}
        renderOption={(option) => (
          <div>
            <div>
              {option.city} | {option.country}
            </div>
            <div className={classes.airport}>{option.name}</div>
          </div>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label={this.props.label}
            margin="none"
            helperText={
              isEmpty(this.props.value) ? null : this.props.value.name
            }
            className={classes.input}
          />
        )}
      />
    );
  }
}

// const airportData = [
//   { city: "Delhi", country: "India", airport: "Safdarjung Airport" },
//   {
//     city: "Delhi",
//     country: "India",
//     airport: "Indira Gandhi International Airport",
//   },
// ];

export default withStyles(styles)(AirportInput);
