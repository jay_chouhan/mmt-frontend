import {
  AppBar,
  Container,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React from "react";
import FlightTakeoffOutlinedIcon from "@material-ui/icons/FlightTakeoffOutlined";
import TrainOutlinedIcon from "@material-ui/icons/TrainOutlined";
import DriveEtaOutlinedIcon from "@material-ui/icons/DriveEtaOutlined";
import DirectionsBusOutlinedIcon from "@material-ui/icons/DirectionsBusOutlined";
import HomeWorkOutlinedIcon from "@material-ui/icons/HomeWorkOutlined";
import Login from "./Login";
import Signup from "./Signup";
import { Link } from "react-router-dom";
import isEmpty from "./utils/isEmpty";
import LoggedInButton from "./LoggedInButton";

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: "#fff",
    padding: "0.5rem",
    position: "relative",
  },
  darkBg: {
    position: "absolute",
    bottom: 0,
    left: 0,
    transform: "translateY(100%)",
    backgroundImage: "linear-gradient(to bottom,#051322,#15457c)",
    height: "300px",
    width: "100%",
    zIndex: "-1",
  },

  nav: {
    display: "flex",
    alignItems: "center",
    gap: "2rem",
    color: "#000",
  },
  label: {
    fontSize: "0.8rem",
    fontWeight: 300,
  },

  navbar: {
    backgroundColor: "#fff",
    boxShadow: "none",
  },
  logo: {
    width: "125px",
  },
  toolBar: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  navLinks: {
    display: "flex",
    gap: "1rem",
    color: "#000",
  },
  icon: {
    fontSize: "2rem",
    color: "#000",
    cursor: "pointer",
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
  },
}));

const Header = (props) => {
  const classes = useStyles();
  return (
    <header className={classes.header}>
      <Container maxWidth="lg" className={classes.navbar} disableGutters>
        <AppBar position="static" className={classes.navbar}>
          <Toolbar className={classes.toolBar}>
            <Link to="/">
              <img src="/logo@2x.png" alt="logo" className={classes.logo} />
            </Link>
            <div className={classes.nav}>
              <div>
                <FlightTakeoffOutlinedIcon
                  color="primary"
                  className={`${classes.icon} ${classes.active}`}
                />
                <Typography className={`${classes.label} ${classes.active}`}>
                  Flights
                </Typography>
              </div>
              <div>
                <TrainOutlinedIcon className={classes.icon} />
                <Typography className={classes.label}>Trains</Typography>
              </div>
              <div>
                <DriveEtaOutlinedIcon className={classes.icon} />
                <Typography className={classes.label}>Cabs</Typography>
              </div>
              <div>
                <DirectionsBusOutlinedIcon className={classes.icon} />
                <Typography className={classes.label}>Bus</Typography>
              </div>
              <div>
                <HomeWorkOutlinedIcon className={classes.icon} />
                <Typography className={classes.label}>Hotels</Typography>
              </div>
            </div>
            <div className={classes.navLinks}>
              {isEmpty(props.user) ? (
                <>
                  <Login verifyUser={props.verifyUser} />
                  <Signup verifyUser={props.verifyUser} />
                </>
              ) : (
                <LoggedInButton user={props.user} logout={props.logout} />
              )}
            </div>
          </Toolbar>
        </AppBar>
      </Container>
      <div className={classes.darkBg}></div>
    </header>
  );
};

export default Header;
